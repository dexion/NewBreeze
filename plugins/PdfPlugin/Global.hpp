/*
    *
    * Global.hpp - Globally used header
    *
*/

#pragma once

#include <QtGui>
#include <QtCore>

#include <unistd.h>
#include "mupdf/fitz.h"

#include "newbreeze.hpp"
